﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTable
{
    public partial class HashtableForm : Form
    {
        CarHashtable hashtable = new CarHashtable();
        string filename = "";
        public HashtableForm()
        {
            InitializeComponent();
        }

        private void RefreshTable()
        {
            dgv.DataSource = null;
            dgv.DataSource = hashtable.ToList();
            dgv.Columns["Brand"].HeaderText = "Бренд";
            dgv.Columns["Number"].HeaderText = "Номер";
            dgv.Columns["FIO"].HeaderText = "ФИО";
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            using (CarForm form = new CarForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    hashtable.Add(form.Car);
                }
                else
                {
                    return;
                }
            }
            RefreshTable();
        }

        private void miEditEdit_Click(object sender, EventArgs e)
        {
            string number;
            using (NumberForm form = new NumberForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    number = form.Number;
                }
                else
                {
                    return;
                }
            }
            Car car = hashtable.Find(number);
            if (car == null)
            {
                MessageBox.Show("Машина с данным номером не найдена");
                return;
            }
            using (CarForm form = new CarForm(car))
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    hashtable.Delete(number);
                    hashtable.Add(form.Car);
                }
                else
                {
                    return;
                }
            }
            RefreshTable();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string number;
            using (NumberForm form = new NumberForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    number = form.Number;
                }
                else
                {
                    return;
                }
            }
            Car car = hashtable.Find(number);
            if (car == null)
            {
                MessageBox.Show("Машина с данным номером не найдена");
                return;
            }
            hashtable.Delete(number);
            RefreshTable();
        }

        private void miSearch_Click(object sender, EventArgs e)
        {
            string number;
            using (NumberForm form = new NumberForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    number = form.Number;
                }
                else
                {
                    return;
                }
            }
            Car car = hashtable.Find(number);
            if (car == null)
            {
                MessageBox.Show("Машина с данным номером не найдена");
                return;
            }
            using (CarForm form = new CarForm(car, true))
            {
                form.ShowDialog();
            }
        }

        private void miFileNew_Click(object sender, EventArgs e)
        {
            hashtable = new CarHashtable();
            RefreshTable();
            filename = "";
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void miOpenFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog form = new OpenFileDialog()) 
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    string fileText = File.ReadAllText(form.FileName);
                    string[] lineArray = fileText.Split(Environment.NewLine.ToCharArray());
                    int i = 0;
                    while (i < lineArray.Length - 2)
                    {
                        Car car = new Car();
                        car.Number = lineArray[i++];
                        car.Brand = lineArray[i++];
                        car.FIO = lineArray[i++];
                        hashtable.Add(car);
                    }
                    RefreshTable();
                }
            }
        }

        private void SaveToFile()
        {
            string fileText = "";
            foreach (Car car in hashtable.ToList())
            {
                fileText += car.Number + "\n";
                fileText += car.Number + "\n";
                fileText += car.Number + "\n";
            }
            File.WriteAllText(filename, fileText);
        }

        private void miSaveFile_Click(object sender, EventArgs e)
        {
            if (filename == "")
            {
                miSaveFileAs_Click(sender, e);
                return;
            }
            SaveToFile();
        }

        private void miSaveFileAs_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog form = new SaveFileDialog())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    filename = form.FileName;
                    SaveToFile();
                }
            }
        }
    }
}
