﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTable
{
    public partial class NumberForm : Form
    {
        public string Number;
        public NumberForm()
        {
            InitializeComponent();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbNumber.Text))
            {
                MessageBox.Show("Введите все данные");
                return;
            }
            Number = tbNumber.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
