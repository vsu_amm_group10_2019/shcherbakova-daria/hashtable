﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{
    public class Car
    {
        public string Number { get; set; }
        public string Brand { get; set; }
        public string FIO { get; set; }
        public static int HashFunc1(string number)
        {
            int hash = 0;
            for (int i = 0; i < number.Length; i++)
            {
                hash += i * number[i];
            }
            return hash;
        }

        public static int HashFunc2(string number)
        {
            int hash = 0;
            for (int i = 0; i < number.Length; i++)
            {
                hash += (number.Length - i) * number[i];
            }
            return hash;
        }

    }
}
