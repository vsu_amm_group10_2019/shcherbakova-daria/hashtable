﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{
    class CarHashtable
    {
        public int Size { get; }
        public int Count { get; set; }
        private Element[] Table { get; set; }

        public List<Car> ToList()
        {
            List<Car> list = new List<Car>();
            foreach (Element elem in Table)
            {
                if (elem != null && !elem.IsDeleted)
                {
                    list.Add(elem.Car);
                }
            }
            return list;
        }
        
        public CarHashtable(int size = 101)
        {
            Size = size;
            Table = new Element[size];
            Count = 0;
        }

        public void TableRehash()
        {
            int deleted = 0;
            foreach (Element elem in Table)
            {
                if (elem != null && elem.IsDeleted)
                {
                    deleted++;
                }
            }
            // Если 50% и больше места занимают удаленные ячейки 
            if (deleted / (Size - Count) >= 0.5)
            {
                Element[] oldTable = Table;
                Table = new Element[Size];
                foreach(Element elem in oldTable)
                {
                    if (elem != null && !elem.IsDeleted)
                    {
                        Add(elem.Car);
                    }
                }
            }
        }

        private int GetIndex(string number, int i)
        {
            return (Car.HashFunc1(number) + i * Car.HashFunc2(number)) % Size;
        }
        public bool Add(Car car)
        {
            TableRehash();
            if (Count >= Size || Find(car.Number) != null)
            {
                return false;
            }
            int i = 1;
            int index = GetIndex(car.Number, i++);
            Element elem = Table[index];
            while (true)
            {
                if (elem == null)
                {
                    Table[index] = new Element();
                    elem = Table[index];
                    elem.Car = car;
                    return true;
                }
                else if (elem.IsDeleted)
                {
                    elem.IsDeleted = false;
                    elem.Car = car;
                    return true;
                }
                index = GetIndex(car.Number, i++);
                elem = Table[index];
            }
        }

        public void Delete(string number)
        {
            Element elem = FindElem(number);
            elem.Car = null;
            elem.IsDeleted = true;
            TableRehash();
        }

        private Element FindElem(string number)
        {
            int i = 1;
            Element elem = Table[GetIndex(number, i++)];
            while (elem != null)
            {
                if (!elem.IsDeleted && elem.Car.Number == number)
                {
                    return elem;
                }
                elem = Table[GetIndex(number, i++)];
            }
            return elem;
        }

        public Car Find(string number)
        {
            Element elem = FindElem(number);
            if (elem == null)
            {
                return null;
            }
            return elem.Car;
        }

        public class Element
        {
            public Car Car { get; set; }
            public bool IsDeleted { get; set; }
        }
    }
}
